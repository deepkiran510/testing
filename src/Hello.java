
public class Hello {

	private int getTotal(String numbers, String delimiter) {
		String phrase = numbers;
		String delims = delimiter;

		String[] tokens = phrase.split(delims);
		
		// convert the numbers to strings
		// add the numbers
		int total = 0;
		for (int i = 0; i < tokens.length ;i++) {
			System.out.println("Num: " + tokens[i]);
			int num = Integer.parseInt(tokens[i]);
			total = total + num;
		}
		System.out.println("Output: " + total);
		return total;
	}
	
	
	
	public int calculate(String number) {
	
		if (number == "") {
			return 0;
		}
		
		if (number.contains(",") || number.contains("\n")) {
			int result = 0;
			if (number.contains(",")) {
				result = this.getTotal(number, ",");
			}
			else if (number.contains("\n")) {
				result = this.getTotal(number, "\n");
			}
			return result;
		}
		
		
		int num = 0;
		num = Integer.parseInt(number);
		return num;
	}
	
}
