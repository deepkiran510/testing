import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestHello {

	@Test
	public void testEmptyStringReturnsZero() {
		Hello s = new Hello();
		int result = s.calculate("");
		assertEquals(0, result);
	}
	@Test
	public void testSingleNumberReturnsValue() {
		Hello s = new Hello();
		int result = s.calculate("550");
		assertEquals(550, result);
	}


}
